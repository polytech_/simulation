import numpy as np
import math
from utils import *
from Bus import Bus

DureeSimu = 40
REPS = 500
REP_ITERATOR = 500


def Debut():
    global DateSimu
    global Echeancier

    newDateBus = np.random.exponential(2) + DateSimu

    Echeancier.append([ArriveBus, newDateBus])
    Echeancier.append([Fin, DureeSimu])
    Echeancier.sort(key=lambda x: x[1], reverse=False)




def Fin():
    global AireBr
    global AireQc
    global AireQr
    global NbBusRep
    global Echeancier
    global compteurBusControles

    global listTpsAttMoyAvtCtrl
    global listTpsAttMoyAvtReparation
    global listTailleMoyFilleC
    global listTailleMoyFilleR
    global listTauxMoyUtilPosteReparation

    global sommeTpsAttMoyAvtCtrl
    global sommeTpsAttMoyAvtRep
    global sommeTauxUtilisationCentreRep
    global sommetailleMoyFilleC
    global sommetailleMoyFilleR

    #Si l'echancier n'est pas vide
    if( Echeancier):
        Echeancier.clear()

    if(NbBus > 0):
        tempsMoyenAvantControle = AireQc / NbBus
        listTpsAttMoyAvtCtrl.append(tempsMoyenAvantControle)
        sommeTpsAttMoyAvtCtrl = sommeTpsAttMoyAvtCtrl + tempsMoyenAvantControle
        # print("Temps d'attente moyen avant contrôle :", round(tempsMoyenAvantControle, 2), "heures")


    if(NbBusRep > 0):
        tempsMoyenAvantReparation = AireQr / NbBusRep
        listTpsAttMoyAvtReparation.append(tempsMoyenAvantReparation)
        sommeTpsAttMoyAvtRep = sommeTpsAttMoyAvtRep + tempsMoyenAvantReparation
        # print("Temps d'attente moyen avant réparation :", round(tempsMoyenAvantReparation, 2), "heures")


    tauxUtilisationCentreRep = AireBr / (2 * DureeSimu)
    sommeTauxUtilisationCentreRep = sommeTauxUtilisationCentreRep + tauxUtilisationCentreRep
    listTauxMoyUtilPosteReparation.append(tauxUtilisationCentreRep)


    #Tailles moyennes des files
    tailleMoyFilleC = AireQc / DureeSimu
    sommetailleMoyFilleC = sommetailleMoyFilleC + tailleMoyFilleC
    listTailleMoyFilleC.append(tailleMoyFilleC)

    tailleMoyFilleR = AireQr / DureeSimu
    sommetailleMoyFilleR = sommetailleMoyFilleR + tailleMoyFilleR
    listTailleMoyFilleR.append(tailleMoyFilleR)

    #Données pour une seule réplication
    # print("Taux d'utilisation du centre de réparation :", round(tauxUtilisationCentreRep, 2))
    # print("Nombre de bus :", NbBus)
    # print("Nombre de bus contrôlés :", compteurBusControles)
    # print("Nombre de bus réparés:", NbBusRep)


    if(REP_ITERATOR == 0):
        moyTpsAttMoyAvtCtrl = sommeTpsAttMoyAvtCtrl / REPS
        moyTpsAttMoyAvtRep = sommeTpsAttMoyAvtRep / REPS
        moyTauxUtilisationCentreRep = sommeTauxUtilisationCentreRep / REPS
        moyTailleMoyFilleC = sommetailleMoyFilleC / REPS
        moyTailleMoyFilleR = sommetailleMoyFilleR / REPS


        sum_avtCtrl = 0
        for moy in listTpsAttMoyAvtCtrl:
            sum_avtCtrl = sum_avtCtrl + math.pow((moy - moyTpsAttMoyAvtCtrl), 2)

        variance_avCtrl = (1 / (REPS - 1)) * sum_avtCtrl


        sum_avtRep = 0
        for moy in listTpsAttMoyAvtReparation:
            sum_avtRep = sum_avtRep + math.pow((moy - moyTpsAttMoyAvtRep), 2)

        variance_avRep = (1 / (REPS - 1)) * sum_avtRep


        sum_TailleMoyFileC = 0
        for moy in listTailleMoyFilleC:
            sum_TailleMoyFileC = sum_TailleMoyFileC + math.pow((moy - moyTailleMoyFilleC), 2)

        variance_tailleMoyFilleC = (1 / (REPS - 1)) * sum_TailleMoyFileC


        sum_TailleMoyFileR = 0
        for moy in listTailleMoyFilleR:
            sum_TailleMoyFileR = sum_TailleMoyFileR + math.pow((moy - moyTailleMoyFilleR), 2)

        variance_tailleMoyFilleR = (1 / (REPS - 1)) * sum_TailleMoyFileR


        sum_TauxUtilCentreRep = 0
        for moy in listTauxMoyUtilPosteReparation:
            sum_TauxUtilCentreRep = sum_TauxUtilCentreRep + math.pow((moy - moyTauxUtilisationCentreRep), 2)

        variance_TauxUtilCentreRep = (1 / (REPS - 1)) * sum_TauxUtilCentreRep


        #1 - alpha = 0.95, n = 500 (infini) => t = 1,645 (https://fr.wikipedia.org/wiki/Loi_de_Student)
        borne_inf_avtCtrl = moyTpsAttMoyAvtCtrl - 1.645 * math.sqrt(variance_avCtrl / REPS)
        borne_sup_avtCtrl = moyTpsAttMoyAvtCtrl + 1.645 * math.sqrt(variance_avCtrl / REPS)

        borne_inf_avtRep = moyTpsAttMoyAvtRep - 1.645 * math.sqrt(variance_avRep / REPS)
        borne_sup_avtRep = moyTpsAttMoyAvtRep + 1.645 * math.sqrt(variance_avRep / REPS)

        borne_inf_tailleMoyFilleC = moyTailleMoyFilleC - 1.645 * math.sqrt(variance_tailleMoyFilleC / REPS)
        borne_sup_tailleMoyFilleC = moyTailleMoyFilleC + 1.645 * math.sqrt(variance_tailleMoyFilleC / REPS)

        borne_inf_tailleMoyFilleR = moyTailleMoyFilleR - 1.645 * math.sqrt(variance_tailleMoyFilleR / REPS)
        borne_sup_tailleMoyFilleR = moyTailleMoyFilleR + 1.645 * math.sqrt(variance_tailleMoyFilleR / REPS)

        borne_inf_TauxUtilCentreRep = moyTauxUtilisationCentreRep - 1.645 * math.sqrt(variance_TauxUtilCentreRep / REPS)
        borne_sup_TauxUtilCentreRep = moyTauxUtilisationCentreRep + 1.645 * math.sqrt(variance_TauxUtilCentreRep / REPS)


        print("["+str(REPS), str(DureeSimu)+"]", "Moyenne TpsAttMoyAvtCtrl :", moyTpsAttMoyAvtCtrl)
        print("["+str(REPS), str(DureeSimu)+"]", "Moyenne TpsAttMoyAvtRep :", moyTpsAttMoyAvtRep)
        print("["+str(REPS), str(DureeSimu)+"]", "Moyenne TauxUtilisationCentreRep :", moyTauxUtilisationCentreRep)
        print("["+str(REPS), str(DureeSimu)+"]", "Moyenne TailleMoyFileC :", moyTailleMoyFilleC )
        print("["+str(REPS), str(DureeSimu)+"]", "Moyenne TailleMoyFileR :", moyTailleMoyFilleR)

        print("\n[" + str(REPS), str(DureeSimu) + "]", "Variance TpsAttMoyAvtCtrl :", variance_avCtrl)
        print("[" + str(REPS), str(DureeSimu) + "]", "Variance TpsAttMoyAvtRep :", variance_avRep)
        print("[" + str(REPS), str(DureeSimu) + "]", "Variance TauxUtilisationCentreRep :", variance_TauxUtilCentreRep)
        print("[" + str(REPS), str(DureeSimu) + "]", "Variance TailleMoyFileC :", variance_tailleMoyFilleC)
        print("[" + str(REPS), str(DureeSimu) + "]", "Variance TailleMoyFileR :", variance_tailleMoyFilleR)

        print("\n["+str(REPS), str(DureeSimu)+"]", "IC : TpsAttMoyAvtCtrl =>", "["+str(borne_inf_avtCtrl), ",", str(borne_sup_avtCtrl)+"]"
              , "taille", borne_sup_avtCtrl - borne_inf_avtCtrl)

        print("["+str(REPS), str(DureeSimu)+"]", "IC : TpsAttMoyAvtRep =>", "["+str(borne_inf_avtRep), ",", str(borne_sup_avtRep)+"]"
              , "taille", borne_sup_avtRep - borne_inf_avtRep)

        print("["+str(REPS), str(DureeSimu)+"]", "IC : TailleMoyFileC =>", "["+str(borne_inf_tailleMoyFilleC), ",", str(borne_sup_tailleMoyFilleC)+"]"
              , "taille", borne_sup_tailleMoyFilleC - borne_inf_tailleMoyFilleC)

        print("["+str(REPS), str(DureeSimu)+"]", "IC : TailleMoyFileR =>", "["+str(borne_inf_tailleMoyFilleR), ",",
              str(borne_sup_tailleMoyFilleR)+"]"
              , "taille", borne_sup_tailleMoyFilleR - borne_inf_tailleMoyFilleR)

        print("["+str(REPS), str(DureeSimu)+"]", "IC : TauxUtilisationCentreRep =>", "["+str(borne_inf_TauxUtilCentreRep), ",",
              str(borne_sup_TauxUtilCentreRep)+"]"
              , "taille", borne_sup_TauxUtilCentreRep - borne_inf_TauxUtilCentreRep)



def MiseAJourDesAire(D1, D2):
    global AireQc
    global AireQr
    global AireBr
    global Qc
    global Qr
    global Br

    AireQc = AireQc + (D2 - D1) * Qc
    AireQr = AireQr  + (D2 - D1) * Qr
    AireBr = AireBr + (D2 - D1) * Br



def Simulateur():
    global DateSimu
    global Echeancier
    global replication
    global Qc
    global Qr
    global Br
    global Bc
    global NbBus
    global NbBusRep
    global compteurBusControles
    global AireQr
    global AireQc
    global AireBr

    Qc = 0
    Qr = 0
    Br = 0
    Bc = 0
    NbBus = 0
    NbBusRep = 0
    compteurBusControles = 0
    AireQr = 0
    AireQc = 0
    AireBr = 0

    DateSimu = 0
    Echeancier.append([Debut, DateSimu])
    while(Echeancier):
        #On récupère le premier couple
        firstPair = Echeancier[0]

        #On récupère la date
        Date = firstPair[1]

        #On met à jour les aires
        MiseAJourDesAire(DateSimu, Date)

        #On met à jour la date de simulation
        DateSimu = Date

        #On exécute l'evt Evt
        firstPair[0]()

        #On retire le couple (Evt, Date) utilisé
        if Echeancier:
            Echeancier.pop(0)



def ArriveBus():
    global NbBus
    global Echeancier
    global DateSimu
    global listBus

    #On calcule la date d'insertion
    newDateBus = np.random.exponential(2) + DateSimu

    if(NbBus > 1):
        listBus.append(Bus(newDateBus, 0, False))
    else:
        listBus.append(Bus(DateSimu, 0, False))


    #On insère le couple l'evt ArriveBus à DateSimu + e(1/2) (newDateBus)
    Echeancier.append([ArriveBus, newDateBus])


    #On incrémente le nb de bus
    NbBus = NbBus + 1
    #print('nbbus', NbBus)

    #On insère le couple l'evt ArriveeFileC à DateSimu
    Echeancier.append([ArriveeFileC, DateSimu])

    # Trier l'echancier par date afin que les evt soient stockés par ordre chronologique
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def ArriveeFileC():
    global Qc
    global Echeancier
    global Bc
    global DateSimu

    #Incrémenter le nbBus dans la file C
    Qc = Qc + 1

    #Si le centre de controle est libre
    if Bc == 0 :
         #Insertion de l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])
    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def AccesControle():
    global Qc
    global Bc
    global DateSimu
    global Echeancier
    global somme
    global compteurBusControles

    #On décrémente le nb de bus dans la file C
    Qc = Qc - 1

    #On occupe le centre de controle
    Bc = 1

    #MàJ de la date grâce à la loi uniforme
    newDate = np.random.uniform(1/4, 13/12) + DateSimu

    # print("Heure de départ", newDate)

    #Insertion dans l'échéancier
    Echeancier.append([DepartControle, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def DepartControle():
    global Bc
    global Echeancier
    global DateSimu
    global Qc

    #On libere le centre de controle
    Bc = 0

    #Si la file C est non vide
    if Qc > 0 :
        #Todo ; DateSime n'était pas présente dans l'échéancier
        #On ajoute l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])

    #On génère un nombre aléatoire entre 0 et 1
    ran = np.random.random()

    #Si c'est supérieur à 0.3 (30%)
    if ran < 0.3:
        #On insère ArriveFileR à DateSimu
        Echeancier.append([ArriveFileR, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def ArriveFileR():
    global Qr
    global NbBusRep
    global Br
    global Echeancier

    #Incrémentation du nb de bus dans la file R
    Qr = Qr + 1

    #Incrémentation du nb de bus réparés
    NbBusRep = NbBusRep + 1

    #Si il y a de la place dans le centre de réparation
    if Br < 2:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def AccesReparation():
    global Qr
    global Br
    global DateSimu
    global Echeancier

    #On décrémente me nb de bus dans la file R
    Qr = Qr - 1
    #On occupe un poste dans le centre de réparation
    Br = Br +1
    #Calculer la date d'insertion DateSimu + U([2.1, 4.5])
    newDate = np.random.uniform(2.1,4.5) + DateSimu

    #Insértion de AccesReparation à newDate
    Echeancier.append([DepartReparation, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def DepartReparation():
    global Br
    global Qr
    global Echeancier
    global DateSimu

    #On libere un emplacement du centre de réparation
    Br = Br -1

    #Si la file R n'est pas vide
    if Qr > 0:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

if __name__ == "__main__":
    while(REP_ITERATOR >= 0):
        Simulateur()
        REP_ITERATOR = REP_ITERATOR - 1
