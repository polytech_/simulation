import numpy as np
from Bus import Bus
from utils import *

DureeSimu = 40
replications = 500



def Debut():
    global DateSimu
    global Echeancier

    newDateBus = np.random.exponential(2) + DateSimu

    Echeancier.append([ArriveBus, newDateBus])
    Echeancier.append([Fin, DureeSimu])



def Fin():
    global AireBr
    global AireQc
    global AireQr
    global NbBusRep
    global Echeancier
    global compteurBusControles
    global sommeTpsAttMoyAvtCtrl
    global sommeTpsAttMoyAvtRep
    global sommeTauxUtilisationCentreRep
    global listTpsMaxAvtCtrl
    global listtpsMaxAvtRep

    #Si l'echancier n'est pas vide
    if( Echeancier):
        Echeancier.clear()


    #Q 3.a.b
    if(compteurBusControles > 0):
        #print("compteurBusControles", compteurBusControles)
        tempsMoyenAvantControle = attAvCtrl / compteurBusControles
        sommeTpsAttMoyAvtCtrl = sommeTpsAttMoyAvtCtrl + tempsMoyenAvantControle
        #print("Temps d'attente moyen avant contrôle :", round(tempsMoyenAvantControle, 2), "heures")
    if(NbBusRep > 0):
        tempsMoyenAvantReparation = attAvRep / NbBusRep
        #print("Temps d'attente moyen avant réparation :", round(tempsMoyenAvantReparation, 2), "heures")
        sommeTpsAttMoyAvtRep = sommeTpsAttMoyAvtRep + tempsMoyenAvantReparation

    listTpsMaxAvtCtrl.append(tpsMaxAvtCtrl)
    listtpsMaxAvtRep.append(tpsMaxAvtRep)

    #Donnes pour une seule réplication
    # tauxUtilisationCentreRep = AireBr / (2 * DureeSimu)
    # sommeTauxUtilisationCentreRep = sommeTauxUtilisationCentreRep + tauxUtilisationCentreRep

    # print("Taux d'utilisation du centre de réparation :", round(tauxUtilisationCentreRep, 2))

    # for element in listBus:
    #     print(element.dateArrivee)

    if(replications == 0):
        print(500, "REPS", DureeSimu, "Heures")
        print("Moyenne TpsAttMoyAvtCtrl", sommeTpsAttMoyAvtCtrl / 500)
        print("Moyenne TpsAttMoyAvtRep", sommeTpsAttMoyAvtRep / 500)
        print("Temps d'attente maximum avant contôle (500 reps) :", Average(listTpsMaxAvtCtrl))
        print("Temps d'attente maximum avant réparation (500 reps) :",  Average(listtpsMaxAvtRep))


def MiseAJourDesAire(D1, D2):
    global AireQc
    global AireQr
    global AireBr
    global Qc
    global Qr
    global Br


    AireQc = AireQc + (D2 - D1) * Qc
    AireQr = AireQr  + (D2 - D1) * Qr
    AireBr = AireBr + (D2 - D1) * Br



def Simulateur():
    global DateSimu
    global Echeancier
    global replication
    global Qc
    global Qr
    global Br
    global Bc
    global NbBus
    global NbBusRep
    global compteurBusControles
    global AireQr
    global AireQc
    global AireBr
    global listBus
    global attAvRep
    global attAvCtrl
    global tpsMaxAvtRep
    global tpsMaxAvtCtrl

    Qc = 0
    Qr = 0
    Br = 0
    Bc = 0
    NbBus = 0
    NbBusRep = 0
    compteurBusControles = 0
    AireQr = 0
    AireQc = 0
    AireBr = 0
    listBus = []
    attAvCtrl = 0
    attAvRep = 0
    tpsMaxAvtRep = 0
    tpsMaxAvtCtrl = 0


    DateSimu = 0
    Echeancier.append([Debut, DateSimu])
    while(Echeancier):
        #On récupère le premier couple
        firstPair = Echeancier[0]

        #On récupère la date
        Date = firstPair[1]

        #On met à jour les aires
        MiseAJourDesAire(DateSimu, Date)

        #On met à jour la date de simulation
        DateSimu = Date

        #On exécute l'evt Evt
        firstPair[0]()

        #On retire le couple (Evt, Date) utilisé
        if Echeancier:
            Echeancier.pop(0)


def ArriveBus():
    global NbBus
    global Echeancier
    global DateSimu
    global listBus

    # On incrémente le nb de bus
    NbBus = NbBus + 1

    listBus.append(Bus(DateSimu, False, False))

    # On calcule la date d'insertion
    newDateBus = np.random.exponential(2) + DateSimu

    #On insère le couple l'evt ArriveBus à DateSimu + e(1/2) (newDateBus)
    Echeancier.append([ArriveBus, newDateBus])

    #On insère le couple l'evt ArriveeFileC à DateSimu
    Echeancier.append([ArriveeFileC, DateSimu])

    # Trier l'echancier par date afin que les evt soient stockés par ordre chronologique
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def ArriveeFileC():
    global Qc
    global Echeancier
    global Bc
    global DateSimu

    #Incrémenter le nbBus dans la file C
    Qc = Qc + 1

    #Si le centre de controle est libre
    if Bc == 0 :
         #Insertion de l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def AccesControle():
    global Qc
    global Bc
    global DateSimu
    global Echeancier
    global attAvCtrl
    global compteurBusControles
    global busControles
    global tpsMaxAvtCtrl

    #On décrémente le nb de bus dans la file C
    Qc = Qc - 1

    #On occupe le centre de controle
    Bc = 1

    #MàJ de la date grâce à la loi uniforme
    newDate = np.random.uniform(1/4, 13/12) + DateSimu

    currentDateArrivee = 0
    for bus in listBus:
        if(bus.estControle == False):
            currentDateArrivee = bus.dateArrivee
            bus.estControle =  True
            compteurBusControles = compteurBusControles + 1
            break

    attAvCtrl = attAvCtrl + (DateSimu - currentDateArrivee)
    if (tpsMaxAvtCtrl < DateSimu - currentDateArrivee):
        tpsMaxAvtCtrl = DateSimu - currentDateArrivee

    #Insertion dans l'échéancier
    Echeancier.append([DepartControle, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def DepartControle():
    global Bc
    global Echeancier
    global DateSimu
    global Qc

    #On libere le centre de controle
    Bc = 0

    #Si la file C est non vide
    if Qc > 0 :
        #On ajoute l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])

    #On génère un nombre aléatoire entre 0 et 1
    ran = np.random.random()
    #Si c'est supérieur à 0.3 (30%)
    if ran < 0.3:
        #On insère ArriveFileR à DateSimu
        Echeancier.append([ArriveFileR, DateSimu])
        for bus in reversed(listBus):
            if (bus.estControle == True):
                bus.estRepare = True
                break

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def ArriveFileR():
    global Qr
    global Br
    global Echeancier
    global dateArrFileR

    #Incrémentation du nb de bus dans la file R
    Qr = Qr + 1

    dateArrFileR = DateSimu

    #Si il y a de la place dans le centre de réparation
    if Br < 2:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def AccesReparation():
    global Qr
    global Br
    global DateSimu
    global NbBusRep
    global Echeancier
    global attAvRep
    global tpsMaxAvtRep

    #On décrémente me nb de bus dans la file R
    Qr = Qr - 1
    #On occupe un poste dans le centre de réparation
    Br = Br +1
    #Calculer la date d'insertion DateSimu + U([2.1, 4.5])
    newDate = np.random.uniform(2.1,4.5) + DateSimu


    for bus in reversed(listBus):
        if (bus.estRepare == True):
            # Incrémentation du nb de bus réparés
            NbBusRep = NbBusRep + 1
            break

    attAvRep = attAvRep + (DateSimu - dateArrFileR)
    if (tpsMaxAvtRep < DateSimu - dateArrFileR):
        tpsMaxAvtRep = DateSimu - dateArrFileR

    #Insértion de AccesReparation à newDate
    Echeancier.append([DepartReparation, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def DepartReparation():
    global Br
    global Qr
    global Echeancier
    global DateSimu

    #On libere un emplacement du centre de réparation
    Br = Br -1

    #Si la file R n'est pas vide
    if Qr > 0:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

if __name__ == "__main__":
    while(replications >= 0):
        Simulateur()
        replications = replications - 1
