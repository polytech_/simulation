
import math
from scipy.stats import expon
import numpy
numpy.set_printoptions(suppress=True)


def getList():
    interArriveList =[]
    dureeCtrList = []
    with open("DonneesControle.txt","rt") as fic:
            lines = fic.readlines()
            preDateArr = 0
            for line in lines:
                dateArrivee = line.split(" ")[0].strip()
                interArriveList.append(float(dateArrivee) - preDateArr)
                preDateArr = float(dateArrivee)

                dureeCtr = line.split(" ")[1].strip()
                dureeCtrList.append(float(dureeCtr))
    return interArriveList , dureeCtrList

def testExponentielle(interArriveList, esperance):
    echantillons = len(interArriveList)
    # le nombre de classe égale la racine du nombre d'échantillons
    classe = int(math.sqrt(echantillons))
    
    oberservationCompteurList = [0 for _ in range(classe)]
    theorieCompteurList = [0 for _ in range(classe)]
    differenceList = [0 for _ in range(classe)]
    
    step = esperance / classe
    classList = [[0,step],[step,2*step],[2*step,4*step],
    [4*step,7*step],[7*step,10*step],[10*step,13*step],
    [13*step,17*step],[17*step,"infini"],]

    # classList = [[0, step], [step, 2*step], [2*step ,3*step], [3*step, 4*step],
    #              [4*step, 5*step], [5*step, 6*step], [6*step, 7*step], [7*step, "infini"]]

    #
    # theorieEffectifInterArrive =[]
    # for _ in range(echantillons):
    #     theorieEffectifInterArrive.append(np.random.exponential(1/esperance) )

    #Nombre d'effectifs observés dans chaque classe (intervalle)
    for i in range(echantillons):
        for j in range(len(classList)):
            if type(classList[j][1]) is str: #le dernier interval
                oberservationCompteurList[j] +=1
            elif interArriveList[i] >= classList[j][0] and interArriveList[i] < classList[j][1] :
                oberservationCompteurList[j] +=1
                break

    #Génération des effectifs théoriques : loi exp
    for j in range(len(classList)):
        if type(classList[j][1]) is str: #le dernier interval
            #Calcul grâce à la fonction de répartition de la loi exponentielle
            prob = expon.cdf(classList[j][0], scale=esperance)
            theorieCompteurList[j] = (1 - prob) * echantillons
        else:
            probMax = expon.cdf(classList[j][1], scale=esperance)
            probMin = expon.cdf(classList[j][0], scale=esperance)
            theorieCompteurList[j] = (probMax - probMin)*echantillons
     

    for i in range(classe):
        differenceList[i] =  ((oberservationCompteurList[i] - theorieCompteurList[i] )**2 )/ theorieCompteurList[i]

    print("Les intervales de toutes les classes")
    printClassList(classList)

    print("Inter-Arrivées : Le nombre d'échantillons ", echantillons)
    print("Inter-Arrivées : La répartition d'effectif théorique", numpy.around(theorieCompteurList, 3))
    print("Inter-Arrivées : La répartition d'effectif observé", numpy.around(oberservationCompteurList, 3))
    print("Inter-Arrivées : La difference pour chaque classee", numpy.around(differenceList, 3))

    difference =0
    for i in range(classe):
        difference += differenceList[i] 
    print("Inter-Arrivées : La différence totale z =", numpy.around(difference, 3))


def FRepartitionUniforme(x, a, b):
    if x < a :
        return 0
    elif x >= b:
        return 1
    return (x-a) / (b-a)


def testUniforme(dureeCtrList, a, b):
    echantillons = len(dureeCtrList)
    # le nombre de classe égale la racine du nombre d'échantillons
    classe = int(math.sqrt(echantillons))
    
    oberservationCompteurList = [0 for _ in range(classe)]
    #Nombre d'effectifs théoriques pour chaque classe (intervalle)
    theorieCompteurList = [0 for _ in range(classe)]
    differenceList = [0 for _ in range(classe)]


    #Taille de l'intervalle / nombre de classes
    step = (b - a) / classe

    classList = [[a,a+step],[a+step,a+2*step],[a+2*step,a+3*step],
    [a+3*step,a+4*step],[a+4*step,a+5*step],[a+5*step,a+6*step],
    [a+6*step,a+7*step],[a+7*step,"infini"],]

    #Nombre d'effectifs observés dans chaque classe (intervalle)
    for i in range(echantillons):
        for j in range(len(classList)):
            if type(classList[j][1]) is str: 
                oberservationCompteurList[j] +=1
            elif dureeCtrList[i] >= classList[j][0] and dureeCtrList[i] < classList[j][1]:
                oberservationCompteurList[j] +=1
                break

    #Génération des effectifs théoriques : loi unif
    for j in range(len(classList)):
        if type(classList[j][1]) is str: #le dernier interval
            prob = FRepartitionUniforme(classList[j][0], a, b)
            theorieCompteurList[j] = (1 - prob) * echantillons
        else:
            probMax = FRepartitionUniforme(classList[j][1], a, b)
            probMin = FRepartitionUniforme(classList[j][0], a, b)
            theorieCompteurList[j] = (probMax - probMin ) * echantillons



    #Calcul de la différence pour chaque classe (intervalle)
    for i in range(classe):
        differenceList[i] =  ((oberservationCompteurList[i] - theorieCompteurList[i]) ** 2) / theorieCompteurList[i]


    print("Les interval de toutes les classees")
    printClassList(classList)

    print("Durées de contrôle : Le nombre d'échantillons ", echantillons)
    print("Durées de contrôle: La répartition d'effectif théorique", numpy.around(theorieCompteurList, 3))
    print("Durées de contrôle: La répartition d'effectif observé", numpy.around(oberservationCompteurList, 3))
    print("Durées de contrôle: La difference pour chaque classee", numpy.around(differenceList, 3))

    difference = 0
    for i in range(classe):
        difference += differenceList[i] 
    print("Durées de contrôle: La différence totale z =", numpy.around(difference, 3))
    
def printClassList(classList):
    for interval in classList:
        print("["+str(interval[0])+","+str(interval[1])+"]")

if __name__ == "__main__":

    interArriveList, dureeCtrList = getList()
    testExponentielle(interArriveList, 2)
    # testUniforme(dureeCtrList, 1/4, 13/12)


