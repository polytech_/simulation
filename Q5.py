import numpy as np
import matplotlib.pyplot as plt

#initialisation des variables
from Bus import Bus
from utils import *

DureeSimu = 10000000 #(infini)
replications = 45

def Debut():
    global DateSimu
    global Echeancier

    newDateBus = np.random.exponential(2) + DateSimu

    Echeancier.append([ArriveBus, newDateBus])
    Echeancier.append([Fin, DureeSimu])
    Echeancier.sort(key=lambda x: x[1], reverse=False)




def Fin():
    global AireBr
    global AireQc
    global AireQr
    global NbBusRep
    global Echeancier
    global compteurBusControles
    global sommeTpsAttMoyAvtCtrl
    global sommeTpsAttMoyAvtRep
    global sommeTauxUtilisationCentreRep
    global sommetailleMoyFilleC
    global sommetailleMoyFilleR
    global tps_att_glob
    global moyennes_tps_att

    #Si l'echancier n'est pas vide
    if( Echeancier):
        Echeancier.clear()

    if(NbBus > 0):
        tempsMoyenAvantControle = AireQc / NbBus
        #print("Temps d'attente moyen avant contrôle :", round(tempsMoyenAvantControle, 4), "heures")
        sommeTpsAttMoyAvtCtrl = sommeTpsAttMoyAvtCtrl + tempsMoyenAvantControle

    if(NbBusRep > 0):
        tempsMoyenAvantReparation = AireQr / NbBusRep
        #print("Temps d'attente moyen avant réparation :", round(tempsMoyenAvantReparation, 4), "heures")
        sommeTpsAttMoyAvtRep = sommeTpsAttMoyAvtRep + tempsMoyenAvantReparation

    tauxUtilisationCentreRep = AireBr / (2 * DureeSimu)
    #print("Taux d'utilisation du centre de réparation :", round(tauxUtilisationCentreRep, 4))

    sommeTauxUtilisationCentreRep = sommeTauxUtilisationCentreRep + tauxUtilisationCentreRep

    #Tailles moyennes des files
    tailleMoyFilleC = AireQc / DureeSimu
    sommetailleMoyFilleC = sommetailleMoyFilleC + tailleMoyFilleC

    tailleMoyFilleR = AireQr / DureeSimu
    sommetailleMoyFilleR = sommetailleMoyFilleR + tailleMoyFilleR

    tps_att_glob.append(tps_att_loc)

    #Donnée pour une réplication
    # print("Nombre de bus :", NbBus)
    # print("Nombre de bus contrôlés :", compteurBusControles)
    # print("Nombre de bus réparés:", NbBusRep)
    # print("DateSimu", DateSimu)
    #print("liste  des tps d'attente : 1 rep", tps_att_loc)
    #print("Nombre de replications : ", replication)

    if(replications == 1):

        #On transpose la matrice pour faire un parcours par colonnes
        transposee = np.array(tps_att_glob, dtype="object").transpose()
        for column in transposee:
            #On calcul la moyenne et on l'ajoute
            moyennes_tps_att.append(Average(column))

        #Premiere moyenne glissante
        glissantes_tps_att.append(moyennes_tps_att[0])
        #Le reste (index > 0)
        for av_index in range(1, len(moyennes_tps_att) - fenetre):
            if(av_index < fenetre):
                values_window = moyennes_tps_att[0: 2 * av_index + 1]
                glissantes_tps_att.append(Average(values_window))
            else:
                values_window = moyennes_tps_att[av_index - fenetre:av_index + fenetre + 1]
                glissantes_tps_att.append(Average(values_window))

        print("moy glissantes", len(glissantes_tps_att))

        plt.xlim(0, bus_break)
        plt.plot(moyennes_tps_att)
        plt.plot(glissantes_tps_att)
        plt.show()

        print("Moyenne TpsAttMoyAvtCtrl", sommeTpsAttMoyAvtCtrl / 500)
        print("Moyenne TpsAttMoyAvtRep", sommeTpsAttMoyAvtRep / 500)
        print("Moyenne TauxUtilisationCentreRep", sommeTauxUtilisationCentreRep / 500)
        print("Moyenne TailleMoyFileC", sommetailleMoyFilleC / 500)
        print("Moyenne TailleMoyFileR", sommetailleMoyFilleR / 500)
        print("tps d'attente toutes rep :")
        print("moyennes tps att toutes rep", moyennes_tps_att)
        # print("moyennes tout court", moyennes_tps_att)
        print("moyennes glissantes", glissantes_tps_att)


def MiseAJourDesAire(D1, D2):
    global AireQc
    global AireQr
    global AireBr
    global Qc
    global Qr
    global Br

    AireQc = AireQc + (D2 - D1) * Qc
    AireQr = AireQr  + (D2 - D1) * Qr
    AireBr = AireBr + (D2 - D1) * Br



def Simulateur():
    global DateSimu
    global Echeancier
    global replication
    global Qc
    global Qr
    global Br
    global Bc
    global NbBus
    global NbBusRep
    global compteurBusControles
    global AireQr
    global AireQc
    global AireBr
    global tps_att_loc
    global listBus

    Qc = 0
    Qr = 0
    Br = 0
    Bc = 0
    NbBus = 0
    NbBusRep = 0
    compteurBusControles = 0
    AireQr = 0
    AireQc = 0
    AireBr = 0

    tps_att_loc = []
    listBus = []
    DateSimu = 0
    Echeancier.append([Debut, DateSimu])
    while(Echeancier):
        if(compteurBusControles == bus_break):
            Echeancier = Echeancier[len(Echeancier)-1:]

        #On récupère le premier couple
        firstPair = Echeancier[0]

        #On récupère la date
        Date = firstPair[1]

        #On met à jour les aires
        MiseAJourDesAire(DateSimu, Date)
        #On met à jour la date de simulation

        DateSimu = Date
        #On exécute l'evt Evt
        firstPair[0]()

        #On retire le couple (Evt, Date) utilisé
        if Echeancier:
            Echeancier.pop(0)



def ArriveBus():
    global NbBus
    global Echeancier
    global DateSimu
    global listBus

    # On incrémente le nb de bus
    NbBus = NbBus + 1

    listBus.append(Bus(DateSimu, False, False))

    # On calcule la date d'insertion
    newDateBus = np.random.exponential(2) + DateSimu

    # On insère le couple l'evt ArriveBus à DateSimu + e(1/2) (newDateBus)
    Echeancier.append([ArriveBus, newDateBus])

    # On insère le couple l'evt ArriveeFileC à DateSimu
    Echeancier.append([ArriveeFileC, DateSimu])

    # Trier l'echancier par date afin que les evt soient stockés par ordre chronologique
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def ArriveeFileC():
    global Qc
    global Echeancier
    global Bc
    global DateSimu

    #Incrémenter le nbBus dans la file C
    Qc = Qc + 1

    #Si le centre de controle est libre
    if Bc == 0 :
         #Insertion de l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])
    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def AccesControle():
    global Qc
    global Bc
    global DateSimu
    global Echeancier
    global somme
    global compteurBusControles
    global tps_att_loc

    #On décrémente le nb de bus dans la file C
    Qc = Qc - 1

    #On occupe le centre de controle
    Bc = 1

    #MàJ de la date grâce à la loi uniforme
    newDate = np.random.uniform(1/4, 13/12) + DateSimu

    currentDateArrivee = 0
    for bus in listBus:
        if (bus.estControle == False):
            currentDateArrivee = bus.dateArrivee
            bus.estControle = True
            compteurBusControles = compteurBusControles + 1
            break

    tps_att_loc.append(DateSimu - currentDateArrivee)

    if(compteurBusControles == bus_break):
        Echeancier.append([Fin, DateSimu])

    #Insertion dans l'échéancier
    Echeancier.append([DepartControle, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def DepartControle():
    global Bc
    global Echeancier
    global DateSimu
    global Qc

    #On libere le centre de controle
    Bc = 0

    #Si la file C est non vide
    if Qc > 0 :
        #On ajoute l'evt AccesControle à DateSimu
        Echeancier.append([AccesControle, DateSimu])

    #On génère un nombre aléatoire entre 0 et 1
    ran = np.random.random()
    #Si c'est supérieur à 0.3 (30%)
    if ran < 0.3:
        #On insère ArriveFileR à DateSimu
        Echeancier.append([ArriveFileR, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def ArriveFileR():
    global Qr
    global NbBusRep
    global Br
    global Echeancier

    #Incrémentation du nb de bus dans la file R
    Qr = Qr + 1

    #Incrémentation du nb de bus réparés
    NbBusRep = NbBusRep + 1

    #Si il y a de la place dans le centre de réparation
    if Br < 2:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def AccesReparation():
    global Qr
    global Br
    global DateSimu
    global Echeancier

    #On décrémente me nb de bus dans la file R
    Qr = Qr - 1
    #On occupe un poste dans le centre de réparation
    Br = Br +1
    #Calculer la date d'insertion DateSimu + U([2.1, 4.5])
    newDate = np.random.uniform(2.1,4.5) + DateSimu

    #Insértion de AccesReparation à newDate
    Echeancier.append([DepartReparation, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def DepartReparation():
    global Br
    global Qr
    global Echeancier
    global DateSimu

    #On libere un emplacement du centre de réparation
    Br = Br -1

    #Si la file R n'est pas vide
    if Qr > 0:
        #On insère AccesReparation à DateSimu
        Echeancier.append([AccesReparation, DateSimu])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

if __name__ == "__main__":
    while(replications > 0):
        Simulateur()
        replications = replications - 1
    #Pour une seule réplication, mettre en commentaire la boucle while et décommenter l'instruction suivante:
    #Simulateur()
