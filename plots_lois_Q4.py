import matplotlib.pyplot as plt
from Q4_a_b import getList

#Parsage du fichier DonnesControl.txt et extraction des données
inter_arv, duree_ctrl = getList()

def inter_arrivees():

    plt.plot(sorted(inter_arv, reverse=False))
    plt.xlabel("Nombre de bus")
    plt.ylabel("Durée d'inter-arrivée")
    plt.show()


def durees_ctrl():

    plt.plot(sorted(duree_ctrl, reverse=False))
    plt.ylabel("Durées de contrôles")
    plt.xlim(-5, 100)
    plt.ylim(0, 2)

    plt.show()



inter_arrivees()
#durees_ctrl()


