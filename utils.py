Bc = 0
Br = 0
Qc = 0
Qr = 0
NbBus = 0
somme = 0

DateSimu = 0
NbBusRep = 0
Echeancier = []
listBus = []

AireQc = 0.0
AireQr = 0.0
AireBr = 0.0

compteurBusControles = 0

listTpsMaxAvtCtrl = []
listtpsMaxAvtRep = []

sommeTpsAttMoyAvtCtrl = 0
sommeTpsAttMoyAvtRep  = 0
sommeTauxUtilisationCentreRep = 0
sommetailleMoyFilleC = 0
sommetailleMoyFilleR = 0

#Données pour intervalles d'estimation
listTpsAttMoyAvtCtrl = []
listTpsAttMoyAvtReparation = []
listTailleMoyFilleC = []
listTailleMoyFilleR = []
listTauxMoyUtilPosteReparation = []

#Bus à partir duquel la simulation s'arrête
bus_break = 250

#Listes contenant :
#les temps d'attente avant contrôle de chaque bus pour chaque réplication
tps_att_loc = []
#Toutes les listes de temps d'attentes de tous les bus pour toutes les réplications
tps_att_glob = []
#Moyennes des temps d'attente sur toutes les réplications pour chaque bus
moyennes_tps_att = []

fenetre = 60
#Lissage de la courbe des moyennes dans moyennes_tps_att
glissantes_tps_att = []



#Fonctions utilitaires
def column(matrix, i):
    return [row[i] for row in matrix]

def Average(lst):
    return sum(lst) / len(lst)
